import 'package:flutter/material.dart';

class SubscriptionList extends StatefulWidget {
  const SubscriptionList({ Key? key }) : super(key: key);

  @override
  State<SubscriptionList> createState() => _SubscriptionListState();
}

class _SubscriptionListState extends State<SubscriptionList> {
  //final _savedSubPatreons = <String>{};

  Widget _buildList(){
    final List<String> entries = <String>['View Current Patreon Packages', 
                                          'Subscribe Patreon Packages', 
                                          'Cancel Patreon Packages'];
    final List<int> colorCodes = <int>[200, 100, 50];
    //final List<Color> colorCodes = <Color>[const Color.fromARGB(224, 251, 252, 1)];

    return ListView.separated(
      padding: const EdgeInsets.all(40.0),
      itemCount: entries.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 180,
          color: Colors.teal[colorCodes[index]],
          child: Center(child: Text('Hi,  ${entries[index]}', style: const TextStyle(fontSize: 16.0))),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

  /* IGNORE THIS !!!! */
  /*return ListView(
  padding: const EdgeInsets.all(16.0),
  children: <Widget>[
    Container(
      height: 50,
      color: const Color(0xFFE0FBFC),   //Colors.amber[600],
      child: const Center(child: Text('View Current Patreon Packages')),
    ),
    Container(
      height: 50,
      color: const Color(0xFFC2DFE3),   //Colors.amber[500],
      child: const Center(child: Text('Subscribe Patreon Packages')),
    ),
    Container(
      height: 50,
      color: const Color(0xFF9DB4C0),   //Colors.amber[100],
      child: const Center(child: Text('Cancel Patreon Packages')),
    ),
  ],
);*/

  @override
  Widget build(BuildContext context) {
    //return Container(color: const Color(0xFFC2DFE3));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Patreon Subscriptions')),
        body: _buildList(),
        //const Text('Hello'),
    );
  }
}
